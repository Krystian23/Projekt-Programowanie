﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;
using System.Data;
using System.Text.RegularExpressions;
using ClassLibrary1;
namespace Projekt
{
    /// <summary>
    /// Logika interakcji dla klasy Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        private string login;
        mysql_connect con = new mysql_connect();
        public Window1(string _login)
        {
            InitializeComponent();
            login = _login;
            Radiobutton2.IsChecked = true;
        }
        /// <summary>
        /// metoda uzupełnia datagrid danymi
        /// </summary>
        /// <param name="command1"></param>
        private void fillgrid(MySqlCommand command1)
        {
            MySqlDataAdapter adp = new MySqlDataAdapter(command1);
            DataTable dt = new DataTable();
            adp.Fill(dt);
            dg_data.ItemsSource = dt.DefaultView;
        }
        /// <summary>
        ///  Metoda inicjalizuje kolumny dla prywatnych użytkowników
        /// </summary>
        private void grid_private_users()
        {
            dg_data.Columns.Add(new DataGridTextColumn() { Header = "Imie", Binding = new Binding("Imie") });
            dg_data.Columns.Add(new DataGridTextColumn() { Header = "Nazwisko", Binding = new Binding("Nazwisko") });
            dg_data.Columns.Add(new DataGridTextColumn() { Header = "Nr telefonu", Binding = new Binding("Telefon") });
            dg_data.Columns.Add(new DataGridTextColumn() { Header = "Zamieszkanie", Binding = new Binding("Zamieszkanie") });
            dg_data.AutoGenerateColumns = false;
        }
        /// <summary>
        /// Metoda pobiera dane z bazy a następnie wypełnia datagrid  dla prywatnych użytkowników
        /// </summary>
        private void private_users()
        {
            try{
                MySqlConnection connection = new MySqlConnection(con.connect());
                connection.Open();
                MySqlCommand command1 = new MySqlCommand("SELECT Imie,Nazwisko,Telefon,Zamieszkanie FROM osoby_prywatne WHERE Login = '"+login+"'", connection);
                fillgrid(command1);

                connection.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        /// <summary>
        /// Metoda inicjalizująca kolumny dla firm
        /// </summary>
        private void grid_companies()
        {
            dg_data.Columns.Add(new DataGridTextColumn() { Header = "Nazwa", Binding = new Binding("Nazwa") });
            dg_data.Columns.Add(new DataGridTextColumn() { Header = "Branza", Binding = new Binding("Branza") });
            dg_data.Columns.Add(new DataGridTextColumn() { Header = "Nr telefonu", Binding = new Binding("Telefon") });
            dg_data.Columns.Add(new DataGridTextColumn() { Header = "Zamieszkanie", Binding = new Binding("Zamieszkanie") });
            dg_data.AutoGenerateColumns = false;
        }
        /// <summary>
        /// Metoda pobiera dane z bazy a następnie wypełnia datagrid  dla firm
        /// </summary>
        private void companies()
        {
            try{
                MySqlConnection connection = new MySqlConnection(con.connect());
                connection.Open();
                MySqlCommand command1 = new MySqlCommand("SELECT Nazwa,Telefon,Zamieszkanie,Branza FROM firmy WHERE Login = '" + login + "'", connection);
                fillgrid(command1);
                connection.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        /// <summary>
        /// Metoda umożliwiająca powrót do okna logowania
        /// </summary>

        private void bt_logout_Click(object sender, RoutedEventArgs e)
        {
            MainWindow window = new MainWindow();
            window.Show();
            this.Close();
        }
        /// <summary>
        /// Metoda zamykająca aplikację
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bt_close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Metoda wyświetla kolumny oraz dane dla prywatnych użytkowników
        /// </summary>

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            dg_data.ItemsSource = null;
            dg_data.Columns.Clear();
            grid_private_users();
            private_users();
            company_grid.Visibility = Visibility.Hidden;
            user_grid.Visibility = Visibility.Visible;
        }
        /// <summary>
        /// Metoda wyświetla kolumny oraz dane dla firm
        /// </summary>
        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {
            dg_data.ItemsSource = null;
            dg_data.Columns.Clear();
            grid_companies();
            companies();
            company_grid.Visibility = Visibility.Visible;
            user_grid.Visibility = Visibility.Hidden;
        }
        /// <summary>
        /// metoda dodaje nowy kontakt do prywatnych użytkowników
        /// </summary>
        private void bt_private_user_Click(object sender, RoutedEventArgs e)
        {
            if (Regex.IsMatch(Input_user_name.Text, @"^\p{L}{1,20}$") &&
                Regex.IsMatch(Input_user_lastname.Text, @"^\p{L}{1,20}$") &&
                Regex.IsMatch(Input_user_town.Text, @"^\p{L}{1,20}$") &&
                Regex.IsMatch(input_user_number.Text, @"^[0-9]{9}$"))
            {
                MySqlConnection connection = new MySqlConnection(con.connect());
                connection.Open();
                MySqlCommand command1 = new MySqlCommand("SELECT Login FROM osoby_prywatne WHERE Telefon = '" + input_user_number.Text + "'", connection);
                MySqlDataReader myReader;
                myReader = command1.ExecuteReader();
                bool user_exists = false;
                while (myReader.Read())
                {
                    if (myReader.GetString(0) == login)
                    {
                        user_exists = true;
                    }
                }
                myReader.Close();

                if (user_exists==false)
                {
                    string sql_insert = "INSERT INTO osoby_prywatne(Imie,Nazwisko,Zamieszkanie,Telefon,Login) VALUES('" + (Input_user_name.Text) + "','" + Input_user_lastname.Text + "','" + Input_user_town.Text + "','" + input_user_number.Text + "','" + login + "')";
                    MySqlCommand command = new MySqlCommand(sql_insert, connection);
                    try
                    {
                        if (command.ExecuteNonQuery() == 1)
                        {
                            MessageBox.Show("Pomyślnie dodałeś nowy kontakt!");
                            dg_data.ItemsSource = null;
                            private_users();
                        }
                        else
                        {
                            MessageBox.Show("Nie udało się dodać nowego kontaktu!");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Taki numer już istnieje!");
                }
            }
            else
            {
                MessageBox.Show("Uzupełnij wszystkie dane!-Numer telefonu powinien się składać z 9 cyfr a reszta pól do 20");
            }
        }
        /// <summary>
        /// metoda dodaje nowy kontakt do firm
        /// </summary>
        private void bt_company_Click(object sender, RoutedEventArgs e)
        {
            if (Regex.IsMatch(input_company_industry.Text, @"^\p{L}{1,20}$") &&
                Regex.IsMatch(input_company_name.Text, @"^\p{L}{1,20}$") &&
                Regex.IsMatch(input_company_town.Text, @"^\p{L}{1,20}$") &&
                Regex.IsMatch(input_company_phone.Text, @"^[0-9]{9}$"))
            {
                MySqlConnection connection = new MySqlConnection(con.connect());
                connection.Open();
                MySqlCommand command1 = new MySqlCommand("SELECT Login FROM firmy WHERE Telefon = '" + input_company_phone.Text + "'", connection);
                MySqlDataReader myReader;
                myReader = command1.ExecuteReader();
                bool user_exists = false;
                while (myReader.Read())
                {
                    if (myReader.GetString(0) == login)
                    {
                        user_exists = true;
                    }
                }
                myReader.Close();

                if (user_exists == false)
                {
                    string sql_insert = "INSERT INTO firmy(Nazwa,Telefon,Zamieszkanie,Branza,Login) VALUES('" + (input_company_name.Text) + "','" + input_company_phone.Text + "','" + input_company_town.Text + "','" + input_company_industry.Text + "','" + login + "')";
                    MySqlCommand command = new MySqlCommand(sql_insert, connection);
                    try
                    {
                        if (command.ExecuteNonQuery() == 1)
                        {
                            MessageBox.Show("Pomyślnie dodałeś nowy kontakt!");
                            dg_data.ItemsSource = null;
                            companies();
                        }
                        else
                        {
                            MessageBox.Show("Nie udało się dodać nowego kontaktu!");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Taki numer już istnieje!");
                }
                connection.Close();
            }
            else
            {
                MessageBox.Show("Uzupełnij wszystkie dane!-Numer telefonu powinien się składać z 9 cyfr a reszta pól do 20");
            }

        }
        /// <summary>
        ///  metoda usuwa wybrany kontakt w datagrid
        /// </summary>
        private void bt_delete_Click(object sender, RoutedEventArgs e)
        {
            var index = dg_data.SelectedIndex;

            if (index != -1)
            {
                DataRowView row = (DataRowView)dg_data.SelectedItems[0];
                var value = row["Telefon"].ToString();
                if (Radiobutton2.IsChecked == true)
                {
                    MySqlConnection connection = new MySqlConnection(con.connect());
                    connection.Open();
                    MySqlCommand command1 = new MySqlCommand("DELETE FROM osoby_prywatne WHERE (Telefon = '" + value + "'AND Login = '"+login + "')", connection);
                    try
                    {
                        if (command1.ExecuteNonQuery() == 1)
                        {
                            MessageBox.Show("Pomyślnie usunąłeś kontakt!");
                            dg_data.ItemsSource = null;
                            private_users();
                        }
                        else
                        {
                            MessageBox.Show("Nie udało się usunąć kontaktu!");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    connection.Close();
                }
                if (RadioButton_1.IsChecked == true)
                {
                    MySqlConnection connection = new MySqlConnection(con.connect());
                    connection.Open();
                    MySqlCommand command1 = new MySqlCommand("DELETE FROM firmy WHERE (Telefon = '" + value + "'AND Login = '" + login + "')", connection);
                    try
                    {
                        if (command1.ExecuteNonQuery() == 1)
                        {
                            MessageBox.Show("Pomyślnie usunąłeś kontakt!");
                            dg_data.ItemsSource = null;
                            companies();
                        }
                        else
                        {
                            MessageBox.Show("Nie udało się usunąć kontaktu!");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    connection.Close();
                }
            }
        }
    }
}
